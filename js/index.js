export {DOMElement} from '/SimpleToDoList/js/functions/DomManipulation.js';
import {Grid} from '/SimpleToDoList/js/grid/tableGrid.js';
import ItemView from '/SimpleToDoList/js/grid/itemView.js';
import infoItemView from '/SimpleToDoList/js/grid/infoItemView.js';
import updatingItemView from '/SimpleToDoList/js/grid/updatingItemView.js'
// import {storage} from '/SimpleToDoList/js/storage/eventLocalStorage.js';
import { apiStorage } from '/SimpleToDoList/js/storage/restApiStorage.js';
import ItemForm from '/SimpleToDoList/js/forms/itemForm.js';
import itemSavingForm from '/SimpleToDoList/js/forms/itemSavingForm.js';
import ItemUpdatingForm from  '/SimpleToDoList/js/forms/itemUpdatingForm.js';


// define item view for tableGrid
const onItemDelete = function(item) {
   apiStorage.remove(item.id)
      .then(() => {
         grid.reset();
         grid.render();
      })
};

const onItemUpdate = function(id, newItem) {
   apiStorage.update(id, newItem)
      .then(()=>{
         grid.render1(id);
         // grid.reset();
         // grid.render();
      })
}

const itemUpdatingView = updatingItemView(ItemUpdatingForm(ItemForm, onItemUpdate));

const ItemGridView = ItemView(onItemDelete, ItemUpdatingForm(ItemForm(), onItemUpdate), infoItemView, itemUpdatingView);

// create and render tableGrid
let grid = Grid(
   apiStorage,
   5,
   ItemGridView
);

grid.render();

document
   .querySelector('.table')
   .appendChild(grid.element);

//  create and render Item form for saving new to-do
const onItemCreate = function saveItem(itemData) {

   let event = {
      name: itemData.name,
      description: itemData.description,
      estimate: itemData.estimate,
      priority: itemData.priority,
      date_of_creation: Date.now(),
   };
   apiStorage.create(event)
      .then(response => {
         grid.reset();
         grid.render();
      });
   // storage.create(event);
   // grid.render(); 
};

let form = itemSavingForm(ItemForm(), onItemCreate); ////////

document
   .querySelector('.input1')
   .appendChild(form);






























