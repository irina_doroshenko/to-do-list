function ItemUpdatingForm(itemForm, onItemUpdate) {
    

    return function (item) {
        
        let itemForm1 = itemForm();
        let formBase = itemForm1.element;

        // debugger
        let formUpdateNode = createFormUpdatingNode();
        let updateBtn = formUpdateNode.querySelectorAll('.form_btn')[0];
        let closeBtn = formUpdateNode.querySelectorAll('.form_btn')[1];



        formBase
            .querySelector(".form_data")
            .appendChild(formUpdateNode);

        formBase
            .querySelector(".form_data_container")
            .classList.add("form_data_container__open");
        formBase
            .classList.add("form__updating");
        

        itemForm1.setFormData(item);

        let onUpdateHandler = function (e) {
            e.stopPropagation();

            let formValidationResponse = itemForm1.validateForm();
            if (!formValidationResponse) return;
    

            let data = itemForm1.getformData();
            console.log (data);
            // data.id = item.id;
            data.date_of_creation = Date.now();

            onItemUpdate(item.id, data);

            itemForm1.clearFormValidation();
        }

        function onCloseHandler() {
            destroyForm();

        }

        function destroyForm(){
            updateBtn.removeEventListener('click',onUpdateHandler);
            closeBtn.removeEventListener("click", onCloseHandler);
            formBase.remove();
            formBase = null;
        }

        updateBtn.addEventListener('click', onUpdateHandler);
        closeBtn.addEventListener('click',  onCloseHandler);

        // return

        return {
            element: formBase,
            closeButton: closeBtn,
        };
    }


}

// templates

function createFormUpdatingNode() {
    const template =`
    <div class="data_btn">
        <div class="form_btn">update</div>
        <div class="form_btn">close</div>
    </div> `;

    let tempNode = document.createElement('div');
    tempNode.innerHTML = template.trim();
    let formUpdateNode = tempNode.firstElementChild;

    return formUpdateNode;
}

export default ItemUpdatingForm;