function selectInputField(params) {

    const { name, options, optionsPreviewText = undefined } = params;

    function createInputNode() {
        let template = `
    <select><select>
    `;
        let tempNode = document.createElement('div');
        tempNode.innerHTML = template.trim();
        let inputNode = tempNode.firstElementChild;
        inputNode.name = name;
        inputNode.classList.add('field');

        if (optionsPreviewText) {
            let optionsPreviewElem = document.createElement('option');
            optionsPreviewElem.id = 'optionsPreview';
            optionsPreviewElem.text = optionsPreviewText;
            optionsPreviewElem.value = '';
            optionsPreviewElem.selected = 'selected';
            optionsPreviewElem.hidden = 'hidden';
            inputNode.appendChild(optionsPreviewElem);
        }

        options.forEach(option => {
            let optionNode = document.createElement('option');
            optionNode.value = option;
            optionNode.text = option;
            inputNode.appendChild(optionNode);
        });

        return inputNode;
    }

    let inputNode = createInputNode();
  

   

    let returnedObj = {
        element: inputNode,
        getValue: function () {
            console.log(inputNode);
            return inputNode.value;
        },
        clear: function () {
            if (inputNode.querySelector('#optionsPreview')) {
                inputNode.querySelector('#optionsPreview').selected = 'selected';
            }
        },
    }

    Object.defineProperty(returnedObj, "element", {
        writable: false
    });

    // Object.freeze(inputNode);

    return returnedObj;
}

export { selectInputField };