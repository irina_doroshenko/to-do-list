function createId(){
    return '_' + Math.random().toString(36).substr(2, 9);
}

let storage = {
    getAll: function() {
       

        return JSON.parse(localStorage.getItem("events") )|| [];
    },

    getAllByQuery: function(limit, skip) {
        let eventlist = JSON.parse(localStorage.getItem("events") )|| [];
        if(!eventlist.length){
            return {
                limit: limit,
                skip: skip,
                count: 0,
                items: [],
            }
        }
        //  let responseEvents = eventlist.slice(skip,skip+limit);
        let responseEvents;
        if (!skip) {
            responseEvents = eventlist.slice(-(skip+limit)).reverse();
        } else {
            responseEvents = eventlist.slice(-(skip+limit), -skip,).reverse();

        }
        let response = {
            limit: limit,
            skip: skip,
            count: eventlist.length,
            items: responseEvents,
        }

        return response;
    },

    create: function(event) {
        let id =  createId();
        event.id = id;
        let eventList = this.getAll();
        eventList.push(event);
        localStorage.setItem('events', JSON.stringify(eventList));
        return id;
    },

    remove: function(eventId){
        let eventList = this.getAll();
        eventList = eventList.filter(function(value){
            return value.id != eventId;
        });
        localStorage.setItem('events', JSON.stringify(eventList));
    }

}

export{storage};