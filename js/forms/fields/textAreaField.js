function textAreaField(params) {
   
    const {name} = params;
    function createtextAreaNode() {
        let template = `
        <textarea></textarea>
        `;
        let tempNode = document.createElement('div');
        tempNode.innerHTML = template.trim();
        let textAreaNode = tempNode.firstElementChild;
        textAreaNode.name = name;
        textAreaNode.classList.add('field');
        return textAreaNode;
    }

    let textArea = createtextAreaNode();
    
    return {
        element: textArea,
        getValue: function() {
            return textArea.value;
        },
        clear: function () {
            textArea.value = '';
        },
    }

}

export { textAreaField };