import { DOMElement } from '/SimpleToDoList/js/index.js'
import { textInputField } from '/SimpleToDoList/js/forms/fields/inputTextField.js'
import { textAreaField } from '/SimpleToDoList/js/forms/fields/textAreaField.js'
import { numberInputField } from '/SimpleToDoList/js/forms/fields/inputNumberField.js'
// import {numberInputField} from '/SimpleToDoList/js/forms/inputNumberField copy.js'
import { selectInputField } from '/SimpleToDoList/js/forms/fields/selectField.js'
import { fieldWithValidation } from '/SimpleToDoList/js/forms/fields/fieldWithValidation.js'
import { Validation } from '/SimpleToDoList/js/functions/Validation.js'


function ItemForm() {

    let formGrid = createFormBase();
    // let formLogo = formGrid.querySelector('.form_logo');
    // let saveBtn = formGrid.querySelector('.form_btn');

    let formFields = [];
    let fieldsContainers = formGrid.querySelectorAll('.data_fields');

    let nameNode = fieldWithValidation(
        textInputField({
            name: 'name',
        }),
        [
            Validation.requiredElement(),
            Validation.subStrElement('%u'),
            Validation.maxLengthElement(32)
        ]
    );

    let estimateNode = fieldWithValidation(
        numberInputField(
            { name: 'estimate' }
        ),
        [
            Validation.requiredElement(), 
            Validation.minNumberValueElement(1)
        ]
    );

    let descriptionNode = fieldWithValidation(
        textAreaField(
            { name: 'description' }
        ),
        [
            Validation.subStrElement('$')
        ]
    );

    let priorityNode = fieldWithValidation(
        selectInputField(
            {
                name: 'priority',
                options: [
                    'high'
                    , 'medium'
                    , 'low'
                ],
                optionsPreviewText: 'select priority',
            },
        ),
        [
            Validation.requiredElement()
        ]
    );

    formFields.push(nameNode, descriptionNode, priorityNode, estimateNode);



    for (let i = 0; i < formFields.length; i++) {
        fieldsContainers[i].appendChild(formFields[i].element);
    }

//    console.log( formFields[0].element)
//     console.log(fieldsContainers[0].querySelector(".field")); указыват на один и тот же объект


    // saveBtn.addEventListener('click', function (e) {


    //     e.stopPropagation();
    //     let validation = true;

    //     for (let i = 0; i < formFields.length; i++) {
    //         formFields[i].clearValidation();
    //         if (!formFields[i].validate()) validation = false;
    //     }

    //     if (!validation) return;

    //     let data = {};

    //     for (let i = 0; i < formGrid.length; i++) {
    //         let input = formGrid[i];
    //         data[input.name] = input.value || undefined;
    //     }

    //     saveItemFunc(data);

    //     formFields.forEach(field => {
    //         field.clear();
    //         field.clearValidation();
    //     });

    // });

    // formLogo.addEventListener('click', function (e) {

    //     DOMElement(formGrid.querySelector('.form_data_container')).toggle(200);
    //     toggleForm(e);
    // });

    //eventListeners functions


    //  function toggleForm(e) {

    //     formGrid
    //         .querySelector('.form_text')
    //         .classList.toggle('open');

    //     formGrid
    //         .querySelector('.form_btn')
    //         .classList.toggle('form_btn__disabled');

    //     formGrid
    //         .querySelector('.form_text')
    //         .classList.toggle('close');

    // }

    //return

    // return formGrid;

    return {
        element: formGrid,
        validateForm: function () {
            let validation = true;

            for (let i = 0; i < formFields.length; i++) {
                formFields[i].clearValidation();
                if (!formFields[i].validate()) validation = false;
            }

            if (!validation) return false;

            return true;
        },
        clearFormValidation: function () {
            formFields.forEach(field => {
                field.clear();
                field.clearValidation();
            });
        },
        getformData: function () {
            console.log (priorityNode.getValue());
            return {
                name: nameNode.getValue() || "",
                estimate: +(estimateNode.getValue()) || undefined,
                description: descriptionNode.getValue() || undefined,
                priority: priorityNode.getValue() || "",
            }
        },
        setFormData: function (data) {
            let fields = formGrid.elements;
            fields["name"].value = data.name;
            fields["estimate"].value = data.estimate;
            fields["description"].value = data.description;
            fields["priority"].value = data.priority;


        }
        
    }
}

//templates

function createFormBase() {

    // <div class="form_logo">
            
    // <div class="form_text open">new todo</div>
     
    // <div class="form_btn form_btn__disabled">add</div>

    // </div>




    const template = `
        <form class="form">
            <div class = "form_data_container">
                <div class="form_data data">
                    <div class="data_fields">
                        <div class="field_label"> name </div>
                    </div>    
                    <div class="data_fields">
                        <div class="field_label">description</div>
                    </div>
                    <div class="data_fields">
                        <div class = "field_label">priority</div>
                    </div> 
                    <div class="data_fields">
                        <div class = "field_label">estimate</div>
                    </div>      
                </div>
            </div>
        </form>
    `;


    let tempNode = document.createElement('div');
    tempNode.innerHTML = template.trim();
    let formBaseNode = tempNode.firstElementChild;

    return formBaseNode;
}

export default ItemForm;
