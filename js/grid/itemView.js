import {DOMElement} from '/SimpleToDoList/js/index.js'

function ItemView(onDelete, itemUpdatingForm, infoItemView, updatingItemView) {
    
    return function(item) {

        //create node
        let eventNode = createEventNode(item);
        const deleteButton = eventNode.querySelector('.element_delete-btn');
        let aditionalEventInfocontainer = eventNode.querySelector('.element_aditional_container');
        let updatingItem;
        // let closeBtn;
      

        let infoItemView1 = infoItemView(item, onEditHandler);
        let aditionalContentNode = infoItemView1.element;


        // render item innfo
        
        renderAditionalContent (aditionalContentNode);


        function renderAditionalContent(element) {
            aditionalEventInfocontainer
                .appendChild(element);
        }

        // event handlers
        const onDeleteHandler = function (e) {
            onDelete(item);
            e.stopPropagation();
        }

        const onToggleHandler = function (e) {
            const toggleTime = 300;
            DOMElement(aditionalEventInfocontainer).toggle(toggleTime);
            setTimeout(() => {
                deleteButton.classList.toggle('element_delete-btn__hidden');
            }, toggleTime);
            setTimeout(() => {
                eventNode
                .querySelector('.element_main')
                .classList.toggle('element_main__open');
            }, toggleTime);

        }

        function onEditHandler(e) {
            updatingItem = updatingItemView(item, onCloseHandler);
            // closeBtn = updatingItem.element.querySelectorAll('.form_btn')[1];
            infoItemView1.destroy();
            renderAditionalContent(updatingItem.element);
        }

        function onCloseHandler(e) {
            e.stopPropagation();
            // console.log(closeBtn);
            // if (e.target && e.target === closeBtn) {
                infoItemView1 = infoItemView(item, onEditHandler);
                updatingItem.destroy();
                renderAditionalContent(infoItemView1.element);
            // }
        }


        //attach eventListeners
        deleteButton.addEventListener('click', onDeleteHandler);

        eventNode
            .querySelector('.element_main')
            .addEventListener('click', onToggleHandler);



        //return obj
        return {
            id: item.id,
            element: eventNode,

            destroy: function() {
                deleteButton.removeEventListener('click', onDeleteHandler);
                // editButton.removeEventListener('click', onEditHandler);
                eventNode.removeEventListener('click', onToggleHandler);
                eventNode.remove();
                eventNode = null;
            }
        }
    }
}



//templates

function createEventNode(event) {
    const template = `
        <div class = "items_element element">
            <div class = "element_main">

                <div class="element_data">
                </div>
                <div class="element_delete-btn">
                </div>
            </div>
            <div class = "element_aditional_container">


            </div>
        </div>
        `;
        // <div class="element_done-btn">
        // </div>

        // <div class = "element_aditional">
                    
        // </div>

    let tempNode = document.createElement('div');
    tempNode.innerHTML = template.trim();
    let eventNode = tempNode.firstElementChild;
    eventNode.querySelector('.element_data').textContent = event.name;
    
    // let aditionalInfo = eventNode.querySelector('.element_aditional');

    // let options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
    // let dateElem = document.createElement('div');
    // dateElem.textContent = `date: ${new Date(event.date_of_creation).toLocaleDateString("en-US", options)};`;
    // aditionalInfo.appendChild(dateElem);

    // if (event.description) {
    //     let descriptionElem = document.createElement('div');
    //     descriptionElem.textContent = `description: ${event.description}`;
    //     aditionalInfo.appendChild(descriptionElem);
    // }

    // let priorityElem = document.createElement('div');
    // priorityElem.textContent = `priority: ${event.priority}`;
    // aditionalInfo.appendChild(priorityElem);

    // let estimateElem = document.createElement('div');
    // estimateElem.textContent = `estimate: ${event.estimate} day(s)`;
    // aditionalInfo.appendChild(estimateElem);


    return eventNode;
}

function createFormNode() {

    let tempNode = document.createElement('div');
    tempNode.classList.add("element_aditional");
    return tempNode;
}


function createItemInfoNode(event) {

    // create node with aditional content
    let aditionalInfo = document.createElement('div');
    aditionalInfo.classList.add("element_aditional");

    let options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
    let dateElem = document.createElement('div');
    dateElem.textContent = `date: ${new Date(event.date_of_creation).toLocaleDateString("en-US", options)};`;
    aditionalInfo.appendChild(dateElem);

    if (event.description) {
        let descriptionElem = document.createElement('div');
        descriptionElem.textContent = `description: ${event.description}`;
        aditionalInfo.appendChild(descriptionElem);
    }

    let priorityElem = document.createElement('div');
    priorityElem.textContent = `priority: ${event.priority}`;
    aditionalInfo.appendChild(priorityElem);

    let estimateElem = document.createElement('div');
    estimateElem.textContent = `estimate: ${event.estimate} day(s)`;
    aditionalInfo.appendChild(estimateElem);

    // create pannel node
    const aditionalPannelTemplate =
    ` <div class = "element_aditional_pannel">
    <div class="element_aditional_btn"> edit </div>
    </div>`;

    let tempNode = document.createElement('div');
    tempNode.innerHTML = aditionalPannelTemplate.trim();
    let pannelNode = tempNode.firstElementChild;

    aditionalInfo.appendChild(pannelNode);

    return aditionalInfo;
}

export default ItemView;