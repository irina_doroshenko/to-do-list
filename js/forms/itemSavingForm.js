import { DOMElement } from '/SimpleToDoList/js/index.js'

function itemSavingForm(itemForm, onSaveItemFunc) {


    let formBase = itemForm.element;
    let formFields = itemForm.fields;

    let formLogoSection = createFormLogo();
    let saveBtn = formLogoSection.querySelector('.form_btn');

    formBase
        .prepend(formLogoSection);


    //attach eventLisners

    saveBtn.addEventListener('click', function (e) {

        e.stopPropagation();

        let formValidationResponse = itemForm.validateForm();
        if (!formValidationResponse) return;

        let data = itemForm.getformData();

        onSaveItemFunc(data);

        itemForm.clearFormValidation();

    });

    formLogoSection.addEventListener('click', function (e) {

        DOMElement(formBase.querySelector('.form_data_container')).toggle(200);
        toggleForm(e);
    });

    //eventListeners functions

    function toggleForm(e) {
        formLogoSection
            .querySelector('.form_text')
            .classList.toggle('open');

        formLogoSection
            .querySelector('.form_btn')
            .classList.toggle('form_btn__disabled');

        formLogoSection
            .querySelector('.form_text')
            .classList.toggle('close');
    }

    return formBase;

}

//templates

function createFormLogo() {
    const template = `
    <div class="form_logo">  
        <div class="form_text open">new todo</div> 
        <div class="form_btn form_btn__disabled">add</div>
    </div>`;

    let tempNode = document.createElement('div');
    tempNode.innerHTML = template.trim();
    let formLogoNode = tempNode.firstElementChild;

    return formLogoNode;
}

export default itemSavingForm;