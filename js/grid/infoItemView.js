
function infoItemView (item, onEditHandler) {

    let contentNode = createItemInfoNode(item);
    const editButton = contentNode
        .querySelector('.element_aditional_btn');
    
    editButton.addEventListener('click', onEditHandler);

    return {
        element: contentNode,
        destroy: function() {
            editButton.removeEventListener('click', onEditHandler);
            contentNode.remove();
            contentNode = null;
        }
    }

}

// templates

function createItemInfoNode(event) {

    // create node with aditional content
    let aditionalInfo = document.createElement('div');
    aditionalInfo.classList.add("element_aditional");

    function createDataNode(header, data) {
        const template = `
        <div class="element_aditional_data">
            <span>${header}: </span>
            <span>${data}</span>
        </div>`;

        let tempNode = document.createElement('div');
        tempNode.innerHTML = template.trim();
        let dataNode = tempNode.firstElementChild;
        return dataNode;
    }


    // date element
    let options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
    let dateString = new Date(event.date_of_creation).toLocaleDateString("en-US", options);
    let dateElem = createDataNode('date',  dateString);
    aditionalInfo.appendChild(dateElem);

    // description element
    if (event.description) {
        let descriptionElem = createDataNode('description', event.description);
        aditionalInfo.appendChild(descriptionElem);
    }

    // priority element
    let priorityElem = createDataNode('priority', event.priority);
    aditionalInfo.appendChild(priorityElem);

    // estimate element
    let estimateElem = createDataNode('estimate', `${event.estimate} day(s)`);
    aditionalInfo.appendChild(estimateElem);

    // create pannel node
    const aditionalPannelTemplate =
    ` <div class = "element_aditional_pannel">
    <div class="element_aditional_btn"> edit </div>
    </div>`;

    let tempNode = document.createElement('div');
    tempNode.innerHTML = aditionalPannelTemplate.trim();
    let pannelNode = tempNode.firstElementChild;

    aditionalInfo.appendChild(pannelNode);

    return aditionalInfo;
}

export default infoItemView;