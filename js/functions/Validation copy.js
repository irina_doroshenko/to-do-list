let obj = {
requiredElement: function () {

    return function(value) {
            if (!value)
                return {
                    success: false,
                    errorString: "field is required",
                }

            return {
                success: true,
                errorString: "",
            }
        }
},


maxLengthElement: function(maxLength) {

    return function (value) {
        if (value.length > maxLength)
            return {
                success: false,
                errorString: `maximal lenght is ${maxLength} symbols`,
            }
        return {
            success: true,
            errorString: "",
        }
    }
}
};
export {Validation};