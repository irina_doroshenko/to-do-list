

const storageUrl = 'http://localhost:3000/tasks';

let apiStorage = {

    getAll: function(limit, skip){

        const requestedURL = `${storageUrl}?limit=${limit}&skip=${skip}`;

        return fetch(requestedURL)
            .then(response => response.json())
            .catch(err => {
                console.log(err);
                return {
                    items: [],
                    total: 0,
                };
            });
    },

    getById: function (id) {
        return fetch(storageUrl + `/${id}`)
            .then(response => {
                if (response.status == 200) {
                    return response.json();
                }
                return new Error("invalid response status code");
            })
            .catch(err => {
                console.log(err);
                return undefined;
            })
    },

    create: function (event) {
        return fetch(storageUrl, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json; charset=UTF-8',
            },
            body: JSON.stringify(event)
        }).then(sucessResponse => {
            if (sucessResponse.status == 201) {
                return sucessResponse.json();
            }
            return new Error("invalid response status code"); 
        }).then(data => data.id)
            .catch(err => {
                console.log(err);
                return; // отрицательный id если не удалось создать
            });
    },

    remove: function (eventId) {
        return fetch (storageUrl+`/${eventId}`, {
            method: "DELETE"
        }).then(sucessResponse => {
            if (sucessResponse.status == 200) return sucessResponse.status;
            return new Error("invalid response status code"); 
        }).catch (err => {
            console.log(err);
            return;
        })

    },
    
    update: function (id, event) {
        return fetch(`${storageUrl}/${id}`, {
            method: "PATCH",
            headers: {
                'Content-Type': 'application/json; charset=UTF-8',
            },
            body: JSON.stringify(event)
        }).then(sucessResponse => {
            if (sucessResponse.status == 200) return sucessResponse.status;
            return new Error("invalid response status code");
        }).catch(err => {
            console.log(err);
            return;
        })

    }

}

export {apiStorage}