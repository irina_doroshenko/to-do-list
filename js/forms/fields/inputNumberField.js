
function numberInputField(params) {

    const {name} = params

    function createInputNode() {
        let template = `
    <input type="number"></input>
    `;
        let tempNode = document.createElement('div');
        tempNode.innerHTML = template.trim();
        let inputNode = tempNode.firstElementChild;
        inputNode.name = name;
        inputNode.classList.add('field');
        return inputNode;
    }

    let inputNode = createInputNode();
    
    return {
        element: inputNode,
        getValue:  function() {
            return inputNode.value;
        },
        clear: function () {
            inputNode.value = '';
        },
    }
}

export { numberInputField };