
function fieldWithValidation(inputField, validators) {

    let errorNode = createErrorNode();

    function createErrorNode() {
        let errorNode = document.createElement('div');
        errorNode.classList.add("field-error-div");
        return errorNode;
    }


    return Object.assign(
        {}, 
        inputField,
        {
            validate: function () {
                for (let i = 0; i < validators.length; i++) {
                    let validatorResponse = validators[i](inputField.getValue());
                    if (validatorResponse) {
                        errorNode.textContent = validatorResponse;
                        inputField.element.classList.add('field__error');
                        inputField.element.after(errorNode);
                        return;
                    }
                }
                return true;
            },
            clearValidation: function () {
                inputField.element.classList.remove('field__error');
                errorNode.remove();
            },
        }
    );

}




export { fieldWithValidation };




