function Grid(storage, limit, itemView) {    
    const LIMIT = limit;
    let skip = 0;
    let gridItems = [];

    //create table skelleton

    let tableGrid = createGridBase(0); // 0 елементов при первом создании таблици

    // get containers from table skelleton for table's elements
    let paginationSection = tableGrid.querySelector('.control-panel_pagination');
    let itemsSection = tableGrid.querySelector('.event-table_items');
 

    // add items elements to the itemsSection of tableGrid
    function renderItems(items) {
        if (gridItems) {
            gridItems.forEach(item => item.destroy())
        }

        gridItems = items.map(itemView);
        gridItems.forEach(item => itemsSection.appendChild(item.element));
    }

    function renderItem(newItem) {
        let previousElementNode;
        let oldItemIndex = gridItems
            .findIndex(item => item.id == newItem.id);
        let oldItem = gridItems[oldItemIndex];
        if (oldItem.element.previousSibling) {
            previousElementNode = oldItem.element.previousSibling;
        }
        gridItems.splice(oldItemIndex,1);
        oldItem.destroy();
        let item = itemView(newItem);
        gridItems.push(item );
        if (previousElementNode) {
            previousElementNode.after(item.element);
            return;
        }
        itemsSection.prepend(item.element);
    }


    function updateCounter(count) {
        tableGrid
            .querySelector('.control-panel_count')
            .querySelector('span')
            .textContent = count
    }

    function updatePaginationState(count) {

        let isHiddenPagination = LIMIT >= count;
        paginationSection.hidden = isHiddenPagination;

        let backwordBtn = paginationSection
            .querySelector('.pagination_left');
        
        if (skip === 0) {
            backwordBtn.classList.add('pagination_left__disabled');
        } else {
            backwordBtn.classList.remove('pagination_left__disabled');
        }

        let forwarddBtn = paginationSection
            .querySelector('.pagination_right');

        if (count - skip <= LIMIT) {
            forwarddBtn.classList.add('pagination_right__disabled');
        } else {
            forwarddBtn.classList.remove('pagination_right__disabled');
        }
    }

    function render() {
        storage
            .getAll(LIMIT, skip)
            .then(response => {
                renderItems(response.items);
                updateCounter(response.total);
                updatePaginationState(response.total);
            })
    }

    function render1 (id) {
        storage
        .getById(id)
        .then(response => {
            if (response) {
                renderItem(response);
            }
        });
    }


    //eventListenersFunctions
    function forwardSlideItems(e) {
        skip += LIMIT;
        render();
    }

    function backwordSlideItems(e) {
        skip -= LIMIT;
        render();
    }

    tableGrid.addEventListener('click', function (e) {
        if (e.target && e.target.className == 'pagination_right') {
            forwardSlideItems(e);
        }

        if (e.target && e.target.className == 'pagination_left') {
            backwordSlideItems(e);
        }
    });


    //return obj
    return {
        element: tableGrid,

        reset: function() {
            skip = 0;
        },

        render: render,
        render1: render1
    }

}


function renderNode(parentNode, createNodeFunk, ...funcParams) {
    parentNode.appendChild(createNodeFunk(...funcParams));
}


//tamplates
function createGridBase(totalItemsCount) {
    const template = `
        <div class="event-table">
            <div class="event-table_items items">
                
            </div>

            <div class="event-table_control-panel control-panel">
                <div class="control-panel_pagination pagination" hidden>

                </div>
               
                 <div class="control-panel_count">

                </div>
            </div>
        </div>
        `;

    let tempNode = document.createElement('div');
    tempNode.innerHTML = template.trim();
    let gridBaseNode = tempNode.firstElementChild;


     // get containers from table skelleton for table's elements
     let paginationSection = gridBaseNode.querySelector('.control-panel_pagination');
     let countSection = gridBaseNode.querySelector('.control-panel_count');     
 
     //add elemenst to the table by request
     renderNode(countSection, createTotalEventNumberNode, totalItemsCount);
     renderNode(paginationSection, createEventsPaginationNode);


    return gridBaseNode;
}


function createEventsPaginationNode() {
    const template = `
        <div class="pagination_container">
            <div class="pagination_left pagination_left__disabled"></div>
            <div class="pagination_right"></div>
        </div>
    `;

    let tempNode = document.createElement('div');
    tempNode.innerHTML = template.trim();
    let paginationNode = tempNode.firstElementChild;
    return paginationNode;
}

function createTotalEventNumberNode(eventListLength) {
    const template = `
        <div>
            total numbers of event:
            <span>${eventListLength}</span>
        </div>
    `;
    let tempNode = document.createElement('div');
    tempNode.innerHTML = template.trim();
    let countNode = tempNode.firstElementChild;
    return countNode;
}

export { Grid };