function updatingItemView(itemUpdatingForm) {

    return function (item, onCloseHandler) {

        let contentNode = createFormNode();
        let updatingForm = itemUpdatingForm(item);
        let updatingFormElem = updatingForm.element;
        let closeBtn = updatingForm.closeButton;
        contentNode.appendChild(updatingFormElem);

        contentNode.addEventListener('click', function(e){
            if(e.target && e.target === closeBtn) {
                onCloseHandler(e);
            }
        });

        return{
            element: contentNode,
            destroy: function() {
                contentNode.removeEventListener('click', onCloseHandler);
                contentNode.remove();
                contentNode = null;
            }
        }
        
    }
}

function createFormNode() {

    let tempNode = document.createElement('div');
    tempNode.classList.add("element_aditional");
    return tempNode;
}

export default updatingItemView;