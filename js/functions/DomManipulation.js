// let domManipulations = {
//     removeAllChildNodes: function (parentNode) {
//         while (parentNode.firstChild) {
//             parentNode.removeChild(parentNode.firstChild);
//         }
//     },

//     attachEvent: function(element, eventName, targetClassName, func){

//         element.addEventListener(eventName, function (e) {
            
//             if (e.target && e.target.className === targetClassName){
//                 func(e)
//             }

//         });

//     }
// }


const getToggleDirection = element => {
    if (element.style.height == 'auto') {
        return -1;
    } 
    
    return 1;
    
}

function DOMElement(element) {
    


    return {
     
        removeChildren: function() {
            while (element.firstChild) {
                element.removeChild(element.firstChild);
            }
        },

        attachEventByClass: function(eventName, targetClassName, func, ...args) {
            element.addEventListener(eventName, function (e) {
                if (e.target && e.target.className === targetClassName){
                    func(e, ...args)
                }
            });
        },

        findParentbyTagName: function(tagName) {
            while (element.parentElement) {
                if (element.parentElement.tagName == tagName) {
                    return element.parentElement
                }
            }
        },

        toggle: function (duration) {
             
            let direction;
            if (element.style.height == '0px' || element.style.height == '') {
                direction = 1;

            } else if (element.style.height == 'auto') {
                direction = -1;
            } 

            let startTime = Date.now();
            let goalHeight = direction > 0 ? element.scrollHeight : 0;
            let startHeight = direction > 0 ? 0 : element.scrollHeight;
            let maxHeight = Math.max(goalHeight, startHeight);

            let timerId = setTimeout(function f() {
                let currentTime = Date.now();
                let percent = (currentTime - startTime) / duration;
                let currentHeight = startHeight + (maxHeight * percent) * direction;
                if (currentTime - startTime >= duration) {
                    console.log(currentHeight);
                    element.style.height = direction > 0 ? 'auto' : '0px';
                    return;
                }
                console.log(currentHeight);
                element.style.height = currentHeight + 'px';
                timerId = setTimeout(f, 25);

            }, 0);
        },

    };
}


// function attachEvent(element, event, className, func) {
//    element.addEventListener(event, function (e) {
//       if (e.target && e.target.className === className) {
//          func(e)
//       }
//    })
// }

// attachEvent(
//    eventListSection, 
//    'click', 
//    'element_delete-btn', 
//    function(e) {
//       storage.remove(e.target.parentNode.dataset.eventId);
//       // let eventList = storage.getAll();
//       // renderEvents(eventContainer,eventList);
//       renderEventListSection();
//    }
// )

export {DOMElement};