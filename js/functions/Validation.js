const Validation = {
    
    requiredElement: function () {
        return function(value) {
            return !value ? "field is required" : null;
        }
    },

    maxLengthElement: function(maxLength) {
        return function (value) {
            return value.length > maxLength ? `maximal lenght is ${maxLength} symbols`: null
        }
    },

    subStrElement: function (subStr) {
        return function (value) {
            return value.indexOf(subStr) >= 0 ? `field cannot contain '${subStr}' value` : null;
        }
    },

    minNumberValueElement: function (min) {
        return function(value) {
            return typeof(+value) != "number" ? `fiel can contain only numeric value` : 
            value < min ? `${min} is minimal value` : null
        }
    }

}


export {Validation};